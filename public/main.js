let copy = document.getElementById("start-script-copy");
let pre = document.getElementById("start-script-pre");
// TODO cleanup
function selectElementText(el, win) {
    win = win || window;
    var doc = win.document, sel, range;
    if (win.getSelection && doc.createRange) {
        sel = win.getSelection();
        range = doc.createRange();
        range.selectNodeContents(el);
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (doc.body.createTextRange) {
        range = doc.body.createTextRange();
        range.moveToElementText(el);
        range.select();
    }
}

copy.addEventListener("click", e => {
	e.preventDefault();
	if (navigator.clipboard) {
		navigator.clipboard.writeText(pre.innerText);
	} else {
		selectElementText(pre);
	}
	return false;
});